<?
	ob_start();
?>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <title>Bol&atilde;o da Copa do Mundo 2018</title>
    </head>
    <style>
        html {
            height: 100%;
        }
        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 15px;
            line-height: 1.50;
            color: #666666;
            background-color: #ffffff;
		}
		hr {
			margin-top: 20px;
			margin-bottom: 20px;
			border: 0;
			border-top: 1px solid #eee;
    }
    p{
      margin-left: 2rem;
    }
    </style>
<body>
<?
	session_start();
	$usuario = $_SESSION['id'];
	if (isset($usuario)){
?>
<?
include 'menu.php'; 
include 'conexao.php';

$dia_atual = date(d, time()-(10800));
$mes_atual = date(m);
$ano_atual = date(Y);
$hora_atual = date(H, time()-(10800));
$min_atual = date(i);
$segundo_atual = date(s);

$data_copa = mktime("23", "59", "00", 06, 14, 2018);
$data_a = mktime($hora_atual, $min_atual, $segundo_atual, $mes_atual, $dia_atual, $ano_atual);

if($data_copa>$data_a){
	$flag = true;
}else{
	$flag = false;
}

if($flag){
$primeiro =	 $_POST['selecao1'];
$segundo =	 $_POST['selecao2'];
$terceiro =	 $_POST['selecao3'];
$quarto =	 $_POST['selecao4'];
if (($primeiro!='0')&&($primeiro!=''))
{
	$query = "select count(*) as count from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 1";
	$res = pg_query($_con, $query);
	$valor = pg_fetch_assoc($res);
	$count = $valor['count'];
	if ($count == 0)
	{
		$query = "insert into tb_classificacao (tb_selecao_id_selecao, tb_apostador_id_apostador, nr_ordem) values ($primeiro, $usuario, 1)";
	}else
	{
		$query = "update tb_classificacao set tb_selecao_id_selecao = $primeiro where nr_ordem = 1 and tb_apostador_id_apostador = $usuario";
	}
	$res = pg_query($_con, $query);
}
$query = "select tb_selecao_id_selecao as selecao1 from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 1";


$res = pg_query($_con, $query);
$valor = pg_fetch_assoc($res);
$selecao1 = $valor['selecao1'];

if (($segundo!='0')&&($segundo!='')){
	$query = "select count(*) as count from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 2";
	$res = pg_query($_con, $query);
	$valor = pg_fetch_assoc($res);
	$count = $valor['count'];
	if ($count == 0){
		$query = "insert into tb_classificacao (tb_selecao_id_selecao, tb_apostador_id_apostador, nr_ordem) values ($segundo, $usuario, 2)";
	}else{
		$query = "update tb_classificacao set tb_selecao_id_selecao = $segundo where nr_ordem = 2 and tb_apostador_id_apostador = $usuario";
	}
	$res = pg_query($_con, $query);
}
$query = "select tb_selecao_id_selecao as selecao2 from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 2";

$res = pg_query($_con, $query);
$valor = pg_fetch_assoc($res);
$selecao2 = $valor['selecao2'];

if (($terceiro!='0')&&($terceiro!='')){
	$query = "select count(*) as count from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 3";
	$res = pg_query($_con, $query);
	$valor = pg_fetch_assoc($res);
	$count = $valor['count'];
	if ($count == 0){
		$query = "insert into tb_classificacao (tb_selecao_id_selecao, tb_apostador_id_apostador, nr_ordem) values ($terceiro, $usuario, 3)";
	}else{
		$query = "update tb_classificacao set tb_selecao_id_selecao = $terceiro where nr_ordem = 3 and tb_apostador_id_apostador = $usuario";
	}
	$res = pg_query($_con, $query);
}
$query = "select tb_selecao_id_selecao as selecao3 from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 3";

$res = pg_query($_con, $query);
$valor = pg_fetch_assoc($res);
$selecao3 = $valor['selecao3'];

if (($quarto!='0')&&($quarto!='')){
	$query = "select count(*) as count from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 4";

	$res = pg_query($_con, $query);
	$valor = pg_fetch_assoc($res);
	$count = $valor['count'];
	if ($count == 0){
		$query = "insert into tb_classificacao (tb_selecao_id_selecao, tb_apostador_id_apostador, nr_ordem) values ($quarto, $usuario, 4)";
	}else{
		$query = "update tb_classificacao set tb_selecao_id_selecao = $quarto where nr_ordem = 4 and tb_apostador_id_apostador = $usuario";
	}
	$res = pg_query($_con, $query);
}
$query = "select tb_selecao_id_selecao as selecao4 from tb_classificacao where tb_apostador_id_apostador = $usuario and nr_ordem = 4";

$res = pg_query($_con, $query);
$valor = pg_fetch_assoc($res);
$selecao4 = $valor['selecao4'];
}
?>
<form name="frm_primeiroscolcoados" method="post">
<?
if($flag){
?>
<div class="alert alert-success text-center" role="alert">
  O palpite dos Primeiros Colocados ficar&aacute; dispon&iacute;vel at&eacute; as 23:59 do dia 14!
</div>
<button type="submit" class="btn btn-primary btn-lg btn-block" style="border-radius: 0;">Apostar</button>
<br>
<div class="form-row">
	<div class="form-group col-md-3">
		<label for="inputState">Campe&atilde;o</label>
		<select name="selecao1" id="selecao1" class="form-control">
		<option value="0">Selecione...</option>
		<?
		//include 'conexao.php';
		$query = "select id_selecao, ds_descricao from tb_selecao order by ds_descricao";
		$res = pg_query($_con, $query);
		while ($valor = pg_fetch_assoc($res)){
			$idSel = $valor['id_selecao'];
			$ds = $valor['ds_descricao'];
			if ($selecao1 == $idSel){
				echo "<option value='$idSel' selected>$ds</option>";
			}else{
				echo "<option value='$idSel'>$ds</option>";
			}
			
		}
		?>
		</select>
	</div>
	<div class="form-group col-md-3">
		<label for="inputState">Vice</label>
		<select name="selecao2" id="selecao2" class="form-control">
		<option value="0">Selecione...</option>
		<?
		//include 'conexao.php';
		$query = "select id_selecao, ds_descricao from tb_selecao order by ds_descricao";
		$res = pg_query($_con, $query);
		while ($valor = pg_fetch_assoc($res)){
			$idSel = $valor['id_selecao'];
			$ds = $valor['ds_descricao'];
			if ($selecao2 == $idSel){
				echo "<option value='$idSel' selected>$ds</option>";
			}else{
				echo "<option value='$idSel'>$ds</option>";
			}
			
		}
		?>
		</select>
	</div>
	<div class="form-group col-md-3">
		<label for="inputState">3&ordm Colocado</label>
		<select name="selecao3" id="selecao3" class="form-control">
		<option value="0">Selecione...</option>
		<?
		//include 'conexao.php';
		$query = "select id_selecao, ds_descricao from tb_selecao order by ds_descricao";
		$res = pg_query($_con, $query);
		while ($valor = pg_fetch_assoc($res)){
			$idSel = $valor['id_selecao'];
			$ds = $valor['ds_descricao'];
			if ($selecao3 == $idSel){
				echo "<option value='$idSel' selected>$ds</option>";
			}else{
				echo "<option value='$idSel'>$ds</option>";
			}
			
		}
		?>
		</select>
	</div>
	<div class="form-group col-md-3">
		<label for="inputState">4&ordm Colocado</label>
		<select name="selecao4" id="selecao4" class="form-control">
		<option value="0">Selecione...</option>
		<?
		//include 'conexao.php';
		$query = "select id_selecao, ds_descricao from tb_selecao order by ds_descricao";
		$res = pg_query($_con, $query);
		while ($valor = pg_fetch_assoc($res)){
			$idSel = $valor['id_selecao'];
			$ds = $valor['ds_descricao'];
			if ($selecao4 == $idSel){
				echo "<option value='$idSel' selected>$ds</option>";
			}else{
				echo "<option value='$idSel'>$ds</option>";
			}
			
		}
		?>
		</select>
	</div>
</div>
<?
}else{
?>
<div class="alert alert-warning" role="alert">
  Apostas encerradas!
</div>
<?
}
?>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</form>
</body>
</html>
<?
	}else{
		header('location: index.php');
	}
	ob_end_flush();
?>