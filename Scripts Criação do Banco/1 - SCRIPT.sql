CREATE TABLE tb_etapa (
  id_etapa INTEGER   NOT NULL ,
  ds_etapa VARCHAR(255)      ,
PRIMARY KEY(id_etapa));




CREATE TABLE tb_selecao (
  id_selecao INT   NOT NULL ,
  ds_descricao VARCHAR(255)   NOT NULL ,
  ds_sigla VARCHAR(255)   NULL ,
  ds_pathBandeira VARCHAR(255)      ,
PRIMARY KEY(id_selecao));




CREATE TABLE tb_apostador (
  id_apostador SERIAL   NOT NULL ,
  ds_nome VARCHAR    ,
  ds_endereco VARCHAR    ,
  login VARCHAR(15)   NOT NULL ,
  senha VARCHAR(300)   NOT NULL ,
  email VARCHAR(255)   NOT NULL ,
  nr_pontos INT    ,
  nr_ranking INT    ,
  nr_telefone VARCHAR(14)   NULL ,
  fl_liberacao INTEGER      ,
PRIMARY KEY(id_apostador));




CREATE TABLE tb_topico (
  id_topico SERIAL   NOT NULL ,
  id_apostador SERIAL   NOT NULL ,
  ds_topico VARCHAR    ,
  nr_respostas INTEGER    ,
  dt_data DATE      ,
PRIMARY KEY(id_topico)  ,
  FOREIGN KEY(id_apostador)
    REFERENCES tb_apostador(id_apostador));


CREATE INDEX tb_topico_FKIndex1 ON tb_topico (id_apostador);


CREATE INDEX IFK_Rel_08 ON tb_topico (id_apostador);


CREATE TABLE tb_msg (
  id_msg SERIAL   NOT NULL ,
  id_topico SERIAL   NOT NULL ,
  id_apostador SERIAL   NOT NULL ,
  ds_msg TEXT    ,
  dt_data DATE      ,
PRIMARY KEY(id_msg)    ,
  FOREIGN KEY(id_apostador)
    REFERENCES tb_apostador(id_apostador),
  FOREIGN KEY(id_topico)
    REFERENCES tb_topico(id_topico));


CREATE INDEX tb_msg_FKIndex1 ON tb_msg (id_apostador);
CREATE INDEX tb_msg_FKIndex2 ON tb_msg (id_topico);


CREATE INDEX IFK_Rel_06 ON tb_msg (id_apostador);
CREATE INDEX IFK_Rel_07 ON tb_msg (id_topico);


CREATE TABLE tb_jogos (
  id_selecao1 INT   NOT NULL ,
  id_selecao2 INT   NOT NULL ,
  id_etapaFK INTEGER   NOT NULL ,
  dt_data DATE   NOT NULL ,
  tm_horario TIME    ,
  nr_golsSel1 INT    ,
  nr_golsSel2 INT    ,
  ds_local VARCHAR(150)      ,
PRIMARY KEY(id_selecao1, id_selecao2, id_etapaFK)      ,
  FOREIGN KEY(id_selecao1)
    REFERENCES tb_selecao(id_selecao),
  FOREIGN KEY(id_selecao2)
    REFERENCES tb_selecao(id_selecao),
  FOREIGN KEY(id_etapaFK)
    REFERENCES tb_etapa(id_etapa));


CREATE INDEX tb_selecao_has_tb_selecao_FKIndex1 ON tb_jogos (id_selecao1);
CREATE INDEX tb_selecao_has_tb_selecao_FKIndex2 ON tb_jogos (id_selecao2);
CREATE INDEX tb_jogos_FKIndex3 ON tb_jogos (id_etapaFK);


CREATE INDEX IFK_selecao1 ON tb_jogos (id_selecao1);
CREATE INDEX IFK_selecao2 ON tb_jogos (id_selecao2);
CREATE INDEX IFK_pertence ON tb_jogos (id_etapaFK);


CREATE TABLE tb_aposta (
  id_etapaFK INTEGER   NOT NULL ,
  d_selecao2FK INT   NOT NULL ,
  id_selecao1FK INT   NOT NULL ,
  id_apostadorFK SERIAL   NOT NULL ,
  nr_golsSel1 INT    ,
  nr_golsSel2 INT    ,
  dt_dataAposta DATE   NOT NULL ,
  tm_horaAposta TIME   NOT NULL   ,
PRIMARY KEY(id_etapaFK, d_selecao2FK, id_selecao1FK, id_apostadorFK)    ,
  FOREIGN KEY(id_apostadorFK)
    REFERENCES tb_apostador(id_apostador),
  FOREIGN KEY(id_selecao1FK, d_selecao2FK, id_etapaFK)
    REFERENCES tb_jogos(id_selecao1, id_selecao2, id_etapaFK));


CREATE INDEX tb_apostador_has_tb_jogos_FKIndex1 ON tb_aposta (id_apostadorFK);
CREATE INDEX tb_apostador_has_tb_jogos_FKIndex2 ON tb_aposta (id_selecao1FK, d_selecao2FK, id_etapaFK);


CREATE INDEX IFK_aposta ON tb_aposta (id_apostadorFK);
CREATE INDEX IFK_contem ON tb_aposta (id_selecao1FK, d_selecao2FK, id_etapaFK);

CREATE TABLE tb_classificacao (
  tb_selecao_id_selecao INTEGER   NOT NULL ,
  tb_apostador_id_apostador INTEGER   NOT NULL ,
  nr_ordem INT   NOT NULL ,
PRIMARY KEY(tb_selecao_id_selecao, tb_apostador_id_apostador)    ,
  FOREIGN KEY(tb_apostador_id_apostador)
    REFERENCES tb_apostador(id_apostador),
  FOREIGN KEY(tb_selecao_id_selecao)
    REFERENCES tb_selecao(id_selecao));

