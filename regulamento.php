<?
	ob_start();
?>
<html lang="pt">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <title>Bolão da Copa do Mundo 2018</title>
    </head>
    <style>
        html {
            height: 100%;
        }
        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 15px;
            line-height: 1.50;
            color: #666666;
            background-color: #ffffff;
		}
		hr {
			margin-top: 20px;
			margin-bottom: 20px;
			border: 0;
			border-top: 1px solid #eee;
    }
    p{
      margin-left: 2rem;
    }
    </style>
<body>
<?
	session_start();
	$usuario = $_SESSION['id'];
	if (isset($usuario)){
?>
<?
	include 'menu.php';
?>
<table>
<tr>
<td>
<p class="text-center"><strong>REGULAMENTO DO BOL&Atilde;O DA COPA DO MUNDO DE 2018</strong></p>

   <p><strong>Regras gerais</strong></p>
   <p>O objetivo do bol&atilde;o &eacute; ter o maior n&uacute;mero de pontos
     feitos a partir de uma tabela de pontua&ccedil;&atilde;o informada no presente
     regulamento.</p>
   <p>O prazo final da inscri&ccedil;&atilde;o para o bol&atilde;o ser&aacute; no
     dia 13 de junho de 2018.</p>
   <p>Ser&atilde;o automaticamente desclassificados deste bol&atilde;o os participantes
     que tentarem desrespeitar qualquer um dos itens deste regulamento, ou ainda,
     utilizar quaisquer meios il&iacute;citos para obter beneficio pr&oacute;prio
     ou para terceiros, hip&oacute;tese em que o terceiro beneficiado tamb&eacute;m
     ser&aacute; automaticamente desclassificado do bol&atilde;o. Ser&aacute; desclassificado,
     tamb&eacute;m, o participante que praticar ato considerado ilegal, il&iacute;cito
     ou que atente contra os objetivos e regras deste concurso.</p>
   <p>Toda e qualquer situa&ccedil;&atilde;o n&atilde;o prevista neste regulamento,
     bem como eventuais casos omissos, ser&atilde;o decididos, exclusivamente,
     pelo Administrador do Bolão.</p>
   <!--
     <p><strong>Deveres dos Organizadores</strong></p>
     <p>Os organizadores devem:<br />
     -Divulgar o montante recebido;<br />
     -Contabilizar a pontua&ccedil;&atilde;o de cada participante;<br />
     -Divulgar a classifica&ccedil;&atilde;o parcial e geral, de acordo com a
     pontua&ccedil;&atilde;o;<br />
     -Premiar os vencedores conforme as normas deste regulamento;<br />
     -Divulgar os vencedores do bol&atilde;o.</p>-->

   <p><strong>Apostas</strong></p>
     <p>As apostas consistem no preenchimento completo da tabela da copa do mundo,
       mostrando o placar de cada jogo.</p>
	   <p>As apostas ficarão disponível até <strong>30 min</strong> antes do jogo.</p>
   <p>Defini&ccedil;&atilde;o de pontua&ccedil;&atilde;o<br />
     - Prevalecer&atilde;o as determina&ccedil;&otilde;es da FIFA, por exemplo,
     em cancelamento de Jogo, desclassifica&ccedil;&otilde;es, resultados e casos
     extraordin&aacute;rios.<br />
     - A partir das oitavas de final, valer&aacute; o resultado do tempo normal, n&atilde;o sendo contabilizados os gols das poss&iacute;veis
     prorroga&ccedil;&otilde;es e penalidades.</p>
   <p>Tabela de pontua&ccedil;&atilde;o<br />
     As regras de pontua&ccedil;&atilde;o seguir&atilde;o os seguintes crit&eacute;rios:<br />

	 <p><strong>Pontuação</strong> </p>
       
	   <p>As regras de pontuação seguirão os seguintes critérios: </p>
	   
	   <p>
       Acerto do vencedor ou empate	03 pontos <br />
       Acerto do vencedor ou empate com placar exato 07 pontos <br />
       Acertando o quarto colocado	08 pontos <br />
       Acertando o terceiro colocado	12 pontos <br />
       Acertando o vice-campe&atilde;o	18 pontos <br />
       Acertando o campe&atilde;o	25 pontos <br/>
    </p>
   
   <p><strong>Distribui&ccedil;&atilde;o de pr&ecirc;mios</strong> </p>
   <p>
     O prêmio de R$ 150,00 ser&aacute; dividido entre os tr&ecirc;s primeiro colocados, de
     acordo com a pontua&ccedil;&atilde;o.<br />
     1&ordm; colocado &#8211; R$ 100,00.<br />
     2&ordm; colocado &#8211; R$ 30,00.<br />
     3&ordm; colocado &#8211; R$ 20,00<br />
     </p>
   </p>
   
   <p><strong>Critério de Desempate</strong></p>
   <p>
   Serão considerados, para fins de desempate os seguintes critérios, na respectiva ordem:<br>
	1. O maior número de acertos de placar exato (vencedor e número de gols de cada seleção);<br>
	2. O maior número de acertos do resultado empate, com placar exato;<br>
	3. Sorteio.<br>
   </p>
   
   </p>
</td>
</tr>
</table> 
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</form>
</body>
</html>
<?
	}else{
		header('location: index.php');
	}
	ob_end_flush();
?>
