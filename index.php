<?php
	ob_start();
?>
<?php
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Bolão da Copa do Mundo 2018</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            padding-top: 50px;
        }

        .panel-login {
            border-color: #ccc;
            -webkit-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
        }

        .panel-login>.panel-heading {
            color: #00415d;
            background-color: #fff;
            border-color: #fff;
            text-align: center;
        }

        .panel-login>.panel-heading a {
            text-decoration: none;
            color: #666;
            font-weight: bold;
            font-size: 15px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login>.panel-heading a.active {
            color: #428bca;
            font-size: 18px;
        }

        .panel-login>.panel-heading hr {
            margin-top: 10px;
            margin-bottom: 0px;
            clear: both;
            border: 0;
            height: 1px;
            background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
            background-image: -moz-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
            background-image: -ms-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
            background-image: -o-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
        }

        .panel-login input[type="text"],
        .panel-login input[type="email"],
        .panel-login input[type="password"] {
            height: 45px;
            border: 1px solid #ddd;
            font-size: 16px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login input:hover,
        .panel-login input:focus {
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border-color: #ccc;
        }

        .btn-login {
            background-color: #59B2E0;
            outline: none;
            color: #fff;
            font-size: 14px;
            height: auto;
            font-weight: normal;
            padding: 14px 0;
            text-transform: uppercase;
            border-color: #59B2E6;
        }

        .btn-login:hover,
        .btn-login:focus {
            color: #fff;
            background-color: #53A3CD;
            border-color: #53A3CD;
        }

        .forgot-password {
            text-decoration: underline;
            color: #888;
        }

        .forgot-password:hover,
        .forgot-password:focus {
            text-decoration: underline;
            color: #666;
        }

        .btn-register {
            background-color: #1CB94E;
            outline: none;
            color: #fff;
            font-size: 14px;
            height: auto;
            font-weight: normal;
            padding: 14px 0;
            text-transform: uppercase;
            border-color: #1CB94A;
        }

        .btn-register:hover,
        .btn-register:focus {
            color: #fff;
            background-color: #1CA347;
            border-color: #1CA347;
        }

        body {
            background-image: url("images/background.jpg");
        }
    </style>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</head>

<body>
	<?php
	session_start();
	if (isset($_SESSION['id'])){
		header("location: aposta.php");
	}
	include 'conexao.php';
	$acao = $_GET['acao'];
	$msg = $_GET['msg'];
	if($acao=='verificar'){
		$login = $_POST['login'];
		$senha = md5($_POST['senha']);
		$query = "select id_apostador, fl_liberacao, login, senha from tb_apostador where (login = '$login' OR email = '$login') AND senha = '$senha'";
		$res = pg_query($_con, $query);
		$valor = pg_fetch_assoc($res);
		$id_pessoa = $valor['id_apostador'];
		$vez = $valor['fl_liberacao'];
		$usr = $valor['login'];
		$psw = $valor['senha'];
		if($id_pessoa != "" && $vez != 0){
			session_start();
			$_SESSION['id'] = $id_pessoa;
			$_SESSION['usuario'] = $login;
			$_SESSION['senha'] = $senha;
			$_SESSION['vez'] = $vez;
			header("location: aposta.php");
		} elseif ($id_pessoa != "" && $vez == 0){
			$msg = "Login não liberado pelo administrador! Favor entrar em contato.";
		} else {
			$msg = "Login/Senha errado(s) ou usuário não encontrado.";
		}
	}
	if ($acao == "inserir"){
		$nome = $_POST['nome'];
		$email = $_POST['email'];
		$endereco = $_POST['endereco'];
		$telefone = $_POST['telefone'];
		$login = $_POST['login'];
		$senha = md5($_POST['senha']);
		$senha2 = md5($_POST['senha2']);
		$status = $_POST['status'];
		$query = "select count(*) as count from tb_apostador where login like '$login'";
		$res = pg_query($_con, $query);
		$valor = pg_fetch_assoc($res);
		$count = $valor['count'];
		if ($count == 0){
			if(($nome == "")&&($login == "")&&($senha == "")&&($email == "")){
				$msg = "Campo Obrigatório não preenchido.";
			}
			if($senha != $senha2){
				$msg = "Senah e Confirmar Senha não conferem.";
			}
			if(($nome != "")&&($login != "")&&($senha != "")&&($email != "")&&($senha == $senha2)){
				$query = "insert into tb_apostador (ds_nome, ds_endereco, login, senha, nr_telefone, fl_liberacao, email, nr_pontos, nr_ranking) values ('$nome', '$endereco', '$login', '$senha', '$telefone', '0', '$email', 0, 0)";
				$res = pg_query($_con, $query);
				if ($res == false){
					$msg = "ERRO: " . pg_errormessage();
				}else{
					$msg="Seu Cadastro Foi Realizado com Sucesso!! Após o pagamento seu login será liberado. Qualquer dúvida entrar em contato com rodrifontes@gmail.com";
					header("Location: index.php?msg=$msg");
				}
			}
		} else{
			$msg = "Login já existente.";
		}
		
	}
	
	?>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3" align="center">
				<img width="200" height="200" src="images/mascote.png" class="rounded mx-auto d-block img-fluid">
			</div>
		</div>
		<br>

		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" <?if($acao!="inserir"){echo "class='active'";} ?> id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" <?if($acao=="inserir"){echo "class='active'";} ?> id="register-form-link">Cadastro</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="index.php?acao=verificar" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="login" id="login" tabindex="1" class="form-control" placeholder="Email ou Usuário" value="">
									</div>
									<div class="form-group">
										<input type="password" name="senha" id="senha" tabindex="2" class="form-control" placeholder="Senha">
									</div>
									<!--
								<div class="form-group text-center">
									<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
									<label for="remember"> Lembre Me</label>
								</div>
								-->
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Logar">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<!--<a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>-->
												</div>
											</div>
										</div>
									</div>
								</form>
								<form id="register-form" action="index.php?acao=inserir" method="post" role="form" style="display: none;">
									<div class="form-group">
										<input type="text" name="nome" id="nome" value='<?=$nome?>' required="true" tabindex="1" class="form-control" placeholder="Nome" value="">
									</div>
									<div class="form-group">
										<input type="text" name="login" id="login" value='<?=$login?>' required="true" tabindex="1" class="form-control" placeholder="Login" value="">
									</div>
									<div class="form-group">
										<input type="email" name="email" id="email" value='<?=$email?>' required="true" tabindex="1" class="form-control" placeholder="E-mail" value="">
									</div>
									<div class="form-group">
										<input type="password" name="senha" id="senha" required="true" tabindex="2" class="form-control" placeholder="Senha">
									</div>
									<div class="form-group">
										<input type="password" name="senha2" id="senha2" required="true" tabindex="2" class="form-control" placeholder="Confirmar Senha">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-login" value="Cadastrar">
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$(function () {

			$('#login-form-link').click(function (e) {
				$("#login-form").delay(100).fadeIn(100);
				$("#register-form").fadeOut(100);
				$('#register-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});
			$('#register-form-link').click(function (e) {
				$("#register-form").delay(100).fadeIn(100);
				$("#login-form").fadeOut(100);
				$('#login-form-link').removeClass('active');
				$(this).addClass('active');
				e.preventDefault();
			});

		});

		<?
		if($acao==inserir){
			echo "$('#register-form').delay(100).fadeIn(100)
			$('#login-form').fadeOut(100)
			$('#login-form-link').removeClass('active')
			$(this).addClass('active')
			e.preventDefault()";
		}
		?>

	</script>

	<noscript>
	!Aviso! O Javascript deve estar ativado para uma correta opera&ccedil;&atilde;o da administra&ccedil;&atilde;o.
	</noscript>
	
	<footer class="footer text-center" style="color:white">
		<p>Desenvolvido por Rodrigo Fontes</p>
		<p>Contato: <a href="mailto:rodrifontes@gmail.com">
		rodrifontes@gmail.com</a>.</p>
	</footer>

</body>

<? if($msg != ""){echo"<script>alert('$msg')</script>";} ?>

</html>
<?
	ob_end_flush();
?>